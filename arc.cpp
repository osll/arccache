#include "arc.h"
#include <ctime>

ARC::ARC(int size): cache_size(size)
{
    T1 = LRU(size);
    T2 = LFU(size);
}

bool ARC::exist(string key)
{
    return (T1.exist(key) || T2.exist(key));
}

bool ARC::put(string key)
{
    if (T1.exist(key))
    {
        T2.put(key);
        // pop from T1
        B1.push_back(key);
    }
    else
    {
        time_t seconds = time(NULL);
        tm* timeinfo = localtime(&seconds);
        T1.put(key,asctime(timeinfo));
        if (T2.exist(key))
        {
            incCapacity(key);
            // pop from T2
            B2.push_back(key);
            return false;
        }
    }
    return true;

}

string ARC::get(string key){
    if (T1.exist(key))
    {
        T1.get(key);
    }
    else
    {
        T2.get(key);
    }
}

void ARC::incCapacity(key)
{
    T2.put(key);
}
