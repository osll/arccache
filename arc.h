#ifndef ARC_H
#define ARC_H
#include "lru.h"
#include "lfu.h"
#include <string>

class ARC
{
public:
    int cache_size;
    LRU T1;
    LFU T2;
    list< string > B1, B2;
    ARC(int size);
    bool exist(string key);
    bool put(string key);
    string get(string key);
    void incCapacity(string key);
};

#endif // ARC_H
