#include "lfu.h"
#include <assert.h>

LFU::LFU(int size):cache_size(size)
{
}

bool LFU::exist(string key)
{
    return (item_map.count(key)>0);
}

void LFU::put(string key)
{
    int capacity = 0;
    map<string, string>::iterator it = item_map.find(key);
    if(it != item_map.end())
    {
        //element found;
        item_list.erase(it->second);
        item_map.erase(it);
        capacity = get(key) ++ ;
    }
    item_list.push_front(make_pair(key,capacity));
    item_map.insert(make_pair(key, item_list.begin()));
}

int LFU::get(string key){
        assert(exist(key));
        map<string, string>::iterator it = item_map.find(key);
        item_list.splice(item_list.begin(), item_list, it->second);
        return it->second->second;
};
