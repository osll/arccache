#include <list>
#include <map>
#include <string>

using namespace std;

class LRU
{
private:
    int cache_size;
    list< pair<string,string> > item_list;
    map<string, string> item_map;
public:
    LRU(int size);

    bool exist(string key);
    void put(string key, string capacity);
    string get(string key);
};
