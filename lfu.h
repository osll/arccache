#ifndef LFU_H
#define LFU_H
#include <list>
#include <map>
#include <string>

class LFU
{
private:
    int cache_size;
    list< pair<string,int> > items_list;
    map<string, int> items_map;
public:
    LFU(int size);

    bool exist(string key);
    void put(string key, string val);
    int get(string key);
};

#endif // LFU_H
