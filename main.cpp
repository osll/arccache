#include <QCoreApplication>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include "arc.h"

using namespace std;

void split(vector<string>& dest, string str, const char* delim)
{
    char* pTempStr = strdup( str.c_str() );
    char* pWord = strtok(pTempStr, delim);
    while(pWord != NULL)
    {
        dest.push_back(pWord);
        pWord = strtok(NULL, delim);
    }
    free(pTempStr);
}

int main(int argc, char *argv[])
{
    //setlocale(LC_ALL, "rus");
    string buff;
    //should be changed
    int size = 50;
    ARC cache = ARC(size);
    ifstream fin("test");
    while(!fin.eof())
    {
        fin.getline(buff, sizeof(buff));
        cout << buff;
        vector<string> VecStr;
        split(VecStr, buff, ' ');
        bool fl = false;
        for(int n=0;n<VecStr.size();n++)
        {
            cout << VecStr[n];
            vector<string> str;
            split(str, VecStr[n], '=');
            for(int i=0;i<str.size();i+=2)
            {
                cout << str[i];

                /*INI- Инициатор
                CDB- команда чтения или записи
                LLBA- логический адрес
                 LEN- длина
                PLBA- физический адрес.*/
                if (str[i] == "CDB")
                {
                        if ((str[i+1] != "00000000000000000000000000000000") || (str[i+1] != "880000000000ea183000000004080000"))
                        {
                            fl = true;
                        }
                        else
                        {
                            fl = false;
                        }
                }
                if (str[i] =="LLBA")
                {
                        if (fl)
                        {
                            if (cache.put(str[i+1]))
                                cout << "In";
                            else
                                cout << "Miss";
                        }
                        else
                        {
                            cache.incCapacity(str[i+1]);
                        }
                }
/*              if (str[i] == "LEN")
                        break;
                if (str[i] =="PLBA")
                        break;
*/
            }
        }
    }
    fin.close();
    return 0;
}
